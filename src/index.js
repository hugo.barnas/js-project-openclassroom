import retrieveContent from "./query";

async function showContent() {
    try {
        //déclaration de la variable content qui récupère la valeur de fonction
        const content = await retrieveContent();
        //création d'une variable qui créé une div

        const elt = document.createElement('div')
        //intégration de la valeur de la fonction dans la div nouvellement créée avec un espace en html en plus


        elt.innerHTML = content.join('<br />')

        //intégration de la div dans le body 

        document.getElementsByTagName('body')[0].appendChild(elt);
    }
    catch (e) {
        console.log("error : ", e)
    }
}

//lancement de la fonction

showContent()