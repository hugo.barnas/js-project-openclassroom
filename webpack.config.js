const path = require('path');

module.exports = {
    mode: "production",
    entry: {
        polyfill: "babel-polyfill",
        //point d'entrée
        app: "./src/index.js"
    },
    output: {
        //bundler dans un fichier final
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"]
                    }
                }
            }
        ]
    }
};